var listParamter = {
	verifyStatus : 0,
	page : 0
};
var listData = null;

var listCell = [ function(data) {
	var idCheckbox = document.createElement('input');
	idCheckbox.setAttribute('type', 'checkbox');
	idCheckbox.setAttribute('name', 'idCheckbox');
	idCheckbox.setAttribute('value', data.id);
	return idCheckbox;
}, function(data) {
	return data.journeyTime;
}, function(data) {
	return data.route;
}, function(data) {
	if (data.verifyStatus > 0) {
		if (data.driver) {
			return data.driver.name;
		} else {
			return '未定';
		}
	} else {
		return '未审核';
	}
}, function(data) {
	if (data.verifyStatus > 0) {
		if (data.vehicle) {
			return data.vehicle.licensePlate;
		} else {
			return '未定';
		}
	} else {
		return '未审核';
	}
}, function(data) {
	return data.remark;
}, function(data) {
	if (data.commitCustomer) {
		return data.commitCustomer.name;
	} else {
		return '无';
	}
}, function(data) {
	return oprIcons(data.id, true, true);
} ];

// 更新列表
var updateList = function(page, verifyStatus) {
	if (undefined != verifyStatus) {
		listParamter.verifyStatus = verifyStatus;
	}
	if (undefined != page) {
		listParamter.page = page;
	}
	AdminJourneyService.getList(JSON.stringify(listParamter), function(data) {
		dwr.util.removeAllRows('list-table');
		dwr.util.addRows('list-table', data.content, listCell);
		updatePage(data);

		listData = data.content;

		$('tbody tr:even').addClass("alt-row");
		$('a[rel*=modal]').facebox();
	});
};

// 编辑
var edit = function(id) {
	if (id) {
		for ( var i = 0; i < listData.length; i++) {
			if (listData[i].id == id) {
				dwr.util.setValues(listData[i]);
				if (listData[i].vehicle) {
					dwr.util.setValue('vehicleLicensePlate', listData[i].vehicle.licensePlate);
				}
				if (listData[i].driver) {
					dwr.util.setValue('driverNumber', listData[i].driver.number);
				}
				$('textarea[name="route"]').text(listData[i].route);
				$('textarea[name="remark"]').text(listData[i].remark);
			}
		}
	}
};

// 删除
var delCustom = function(ids) {
	AdminJourneyService.del(JSON.stringify(ids), function(data) {
		if (data) {
			updateList();
		}
	});
};

// 保存
var save = function() {
	AdminJourneyService.save(JSON.stringify($('#facebox').find('form').serializeObject()),
			function(data) {
				if (data) {
					showMessage($('#facebox').find('form'), '保存成功', 'success');
					updateList();
				} else {
					showMessage($('#facebox').find('form'), '填写有误，请检查', 'error');
				}
			});
};

$(window).load(function() {
	// 侧边栏点击事件
	$('.admin-journey-add').click(function() {
		$(".button-editor").click();
	});
	$('.admin-journey-available').click(function() {
		$('.journey-available').click();
	});
	$('.admin-journey-unverified').click(function() {
		$('.journey-unverified').click();
	});
	$('.admin-journey-unpassed').click(function() {
		$('.journey-unpassed').click();
	});
	$('.admin-journey-unavailable').click(function() {
		$('.journey-unavailable').click();
	});

	// 标签栏点击事件
	$('.journey-available').click(function() {
		updateList(0, 2);
	});
	$('.journey-unverified').click(function() {
		updateList(0, 0);
	});
	$('.journey-unpassed').click(function() {
		updateList(0, 1);
	});
	$('.journey-unavailable').click(function() {
		updateList(0, 3);
	});

	// 通过hash判断要进入的功能
	if ("#add" == window.location.hash) {
		$('.admin-journey-available').click();
		$(".button-editor").click();
	} else if ("#unverified" == window.location.hash) {
		$('.admin-journey-unverified').click();
	} else if ('#unpassed' == window.location.hash) {
		$('.admin-journey-unpassed').click();
	} else if ('#unavailable' == window.location.hash) {
		$('.admin-journey-unavailable').click();
	} else {
		$('.admin-journey-available').click();
	}
});
