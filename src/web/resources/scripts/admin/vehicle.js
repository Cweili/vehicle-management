var listParamter = {
	type : 0,
	page : 0
};
var listData = null;

var listCell = [ function(data) {
	var idCheckbox = document.createElement('input');
	idCheckbox.setAttribute('type', 'checkbox');
	idCheckbox.setAttribute('name', 'idCheckbox');
	idCheckbox.setAttribute('value', data.id);
	return idCheckbox;
}, function(data) {
	return data.licensePlate;
}, function(data) {
	return data.model;
}, function(data) {
	return dateString(data.registrationDate);
}, function(data) {
	return dateString(data.buyDate);
}, function(data) {
	return (data.price / 100) + ' 元';
}, function(data) {
	return oprIcons(data.id, true, true);
} ];

// 更新列表
var updateList = function(page, type) {
	if (undefined != type) {
		listParamter.type = type;
	}
	if (undefined != page) {
		listParamter.page = page;
	}
	AdminVehicleService.getList(JSON.stringify(listParamter), function(data) {
		dwr.util.removeAllRows('list-table');
		dwr.util.addRows('list-table', data.content, listCell);
		updatePage(data);

		listData = data.content;

		$('tbody tr:even').addClass("alt-row");
		$('a[rel*=modal]').facebox();
	});
};

// 编辑
var edit = function(id) {
	if (id) {
		for ( var i = 0; i < listData.length; i++) {
			if (listData[i].id == id) {
				dwr.util.setValues(listData[i]);
				dwr.util.setValues({
					'registrationDateYear' : listData[i].registrationDate.getFullYear(),
					'registrationDateMonth' : listData[i].registrationDate.getMonth() + 1,
					'registrationDateDate' : listData[i].registrationDate.getDate(),
					'buyDateYear' : listData[i].buyDate.getFullYear(),
					'buyDateMonth' : listData[i].buyDate.getMonth() + 1,
					'buyDateDate' : listData[i].buyDate.getDate(),
					'price' : listData[i].price / 100
				});
			}
		}
	}
};

// 删除
var delCustom = function(ids) {
	AdminVehicleService.del(JSON.stringify(ids), function(data) {
		if (data) {
			updateList();
		}
	});
};

// 保存
var save = function() {
	AdminVehicleService.save(JSON.stringify($('#facebox').find('form').serializeObject()),
			function(data) {
				if (data) {
					showMessage($('#facebox').find('form'), '保存成功', 'success');
					updateList();
				} else {
					showMessage($('#facebox').find('form'), '填写有误，请检查', 'error');
				}
			});
};

$(window).load(function() {
	// 侧边栏点击事件
	$('.admin-vehicle-add').click(function() {
		$(".button-editor").click();
	});
	$('.admin-vehicle-small').click(function() {
		$('.vehicle-small').click();
	});
	$('.admin-vehicle-large').click(function() {
		$('.vehicle-large').click();
	});
	$('.admin-vehicle-commerical').click(function() {
		$('.vehicle-commerical').click();
	});

	// 标签栏点击事件
	$('.vehicle-small').click(function() {
		updateList(0, 0);
	});
	$('.vehicle-large').click(function() {
		updateList(0, 1);
	});
	$('.vehicle-commerical').click(function() {
		updateList(0, 2);
	});

	// 通过hash判断要进入的功能
	if ("#add" == window.location.hash) {
		$('.admin-vehicle-small').click();
		$(".button-editor").click();
	} else if ('#large' == window.location.hash) {
		$('.admin-vehicle-large').click();
	} else if ('#commerical' == window.location.hash) {
		$('.admin-vehicle-commerical').click();
	} else {
		$('.admin-vehicle-small').click();
	}
});
