var listParamter = {
	type : 0,
	page : 0
};
var listData = null;

var listCell = [ function(data) {
	var idCheckbox = document.createElement('input');
	idCheckbox.setAttribute('type', 'checkbox');
	idCheckbox.setAttribute('name', 'idCheckbox');
	idCheckbox.setAttribute('value', data.id);
	return idCheckbox;
}, function(data) {
	return data.name;
}, function(data) {
	return data.number;
}, function(data) {
	return data.sex == 0 ? '男' : '女';
}, function(data) {
	return dateString(data.joinDate);
}, function(data) {
	return data.idCard;
}, function(data) {
	return data.phone;
}, function(data) {
	return oprIcons(data.id, true, true);
} ];

// 更新列表
var updateList = function(page, type) {
	if (undefined != type) {
		listParamter.type = type;
	}
	if (undefined != page) {
		listParamter.page = page;
	}
	AdminCustomerService.getList(JSON.stringify(listParamter), function(data) {
		dwr.util.removeAllRows('list-table');
		dwr.util.addRows('list-table', data.content, listCell);
		updatePage(data);

		listData = data.content;

		$('tbody tr:even').addClass("alt-row");
		$('a[rel*=modal]').facebox();
	});
};

// 编辑
var edit = function(id) {
	if (id) {
		for ( var i = 0; i < listData.length; i++) {
			if (listData[i].id == id) {
				dwr.util.setValues(listData[i]);
				dwr.util.setValues({
					'joinDateYear' : listData[i].joinDate.getFullYear(),
					'joinDateMonth' : listData[i].joinDate.getMonth() + 1,
					'joinDateDate' : listData[i].joinDate.getDate(),
					'birthdayYear' : listData[i].birthday.getFullYear(),
					'birthdayMonth' : listData[i].birthday.getMonth() + 1,
					'birthdayDate' : listData[i].birthday.getDate(),
				});
			}
		}
	}
};

// 删除
var delCustom = function(ids) {
	AdminCustomerService.del(JSON.stringify(ids), function(data) {
		if (data) {
			updateList();
		}
	});
};

// 保存
var save = function() {
	AdminCustomerService.save(JSON.stringify($('#facebox').find('form').serializeObject()), function(
			data) {
		if (data) {
			showMessage($('#facebox').find('form'), '保存成功', 'success');
			updateList();
		} else {
			showMessage($('#facebox').find('form'), '填写有误，请检查', 'error');
		}
	});
};

$(window).load(function() {
	// 侧边栏点击事件
	$('.admin-customer-add').click(function() {
		$(".button-editor").click();
	});
	$('.admin-customer-normal').click(function() {
		$('.customer-normal').click();
	});
	$('.admin-customer-driver').click(function() {
		$('.customer-driver').click();
	});
	$('.admin-customer-admin').click(function() {
		$('.customer-admin').click();
	});

	// 标签栏点击事件
	$('.customer-normal').click(function() {
		updateList(0, 0);
	});
	$('.customer-driver').click(function() {
		updateList(0, 1);
	});
	$('.customer-admin').click(function() {
		updateList(0, 9);
	});

	// 通过hash判断要进入的功能
	if ("#add" == window.location.hash) {
		$('.admin-customer-normal').click();
		$(".button-editor").click();
	} else if ('#driver' == window.location.hash) {
		$('.admin-customer-driver').click();
	} else if ('#admin' == window.location.hash) {
		$('.admin-customer-admin').click();
	} else {
		$('.admin-customer-normal').click();
	}
});
