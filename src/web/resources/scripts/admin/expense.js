var listParamter = {
	verifyStatus : 0,
	page : 0
};
var listData = null;

var listCell = [ function(data) {
	var idCheckbox = document.createElement('input');
	idCheckbox.setAttribute('type', 'checkbox');
	idCheckbox.setAttribute('name', 'idCheckbox');
	idCheckbox.setAttribute('value', data.id);
	return idCheckbox;
}, function(data) {
	switch (data.classification) {
	case 0:
		return '燃油费';
	case 1:
		return '维护保养';
	case 2:
		return '故障维修';
	case 3:
		return '年检费';
	case 4:
		return '保险费';
	}
}, function(data) {
	return dateString(data.commitDate);
}, function(data) {
	return (data.price / 100) + ' 元';
}, function(data) {
	return data.vehicle.licensePlate;
}, function(data) {
	if (data.commitCustomer) {
		return data.commitCustomer.name;
	} else {
		return '无';
	}
}, function(data) {
	if (data.commitDate) {
		return dateString(data.commitDate);
	} else {
		return '无';
	}
}, function(data) {
	return oprIcons(data.id, true, true);
} ];

// 更新列表
var updateList = function(page, verifyStatus) {
	if (undefined != verifyStatus) {
		listParamter.verifyStatus = verifyStatus;
	}
	if (undefined != page) {
		listParamter.page = page;
	}
	AdminExpenseService.getList(JSON.stringify(listParamter), function(data) {
		dwr.util.removeAllRows('list-table');
		dwr.util.addRows('list-table', data.content, listCell);
		updatePage(data);

		listData = data.content;

		$('tbody tr:even').addClass("alt-row");
		$('a[rel*=modal]').facebox();
	});
};

// 编辑
var edit = function(id) {
	if (id) {
		for ( var i = 0; i < listData.length; i++) {
			if (listData[i].id == id) {
				dwr.util.setValues(listData[i]);
				dwr.util.setValues({
					'vehicleLicensePlate' : listData[i].vehicle.licensePlate,
					'price' : listData[i].price / 100
				});
				$('textarea[name="remark"]').text(listData[i].remark);
			}
		}
	}
};

// 删除
var delCustom = function(ids) {
	AdminExpenseService.del(JSON.stringify(ids), function(data) {
		if (data) {
			updateList();
		}
	});
};

// 保存
var save = function() {
	AdminExpenseService.save(JSON.stringify($('#facebox').find('form').serializeObject()),
			function(data) {
				if (data) {
					showMessage($('#facebox').find('form'), '保存成功', 'success');
					updateList();
				} else {
					showMessage($('#facebox').find('form'), '填写有误，请检查', 'error');
				}
			});
};

$(window).load(function() {
	// 侧边栏点击事件
	$('.admin-expense-unverified').click(function() {
		$('.expense-unverified').click();
	});
	$('.admin-expense-unpassed').click(function() {
		$('.expense-unpassed').click();
	});
	$('.admin-expense-passed').click(function() {
		$('.expense-passed').click();
	});

	// 标签栏点击事件
	$('.expense-unverified').click(function() {
		updateList(0, 0);
	});
	$('.expense-unpassed').click(function() {
		updateList(0, 1);
	});
	$('.expense-passed').click(function() {
		updateList(0, 2);
	});

	// 通过hash判断要进入的功能
	if ('#unpassed' == window.location.hash) {
		$('.admin-expense-unpassed').click();
	} else if ('#passed' == window.location.hash) {
		$('.admin-expense-passed').click();
	} else {
		$('.admin-expense-unverified').click();
	}
});
