var listParamter = {
	verifyStatus : 0,
	page : 0
};
var listData = null;

var listCell = [ function(data) {
	return data.journeyTime;
}, function(data) {
	return data.route;
}, function(data) {
	if (data.verifyStatus > 0) {
		if (data.driver) {
			return data.driver.name;
		} else {
			return '未定';
		}
	} else {
		return '未审核';
	}
}, function(data) {
	if (data.verifyStatus > 0) {
		if (data.vehicle) {
			return data.vehicle.licensePlate;
		} else {
			return '未定';
		}
	} else {
		return '未审核';
	}
}, function(data) {
	return data.remark;
} ];

// 更新列表
var updateList = function(page, verifyStatus) {
	if (undefined != verifyStatus) {
		listParamter.verifyStatus = verifyStatus;
	}
	if (undefined != page) {
		listParamter.page = page;
	}
	JourneyService.getList(JSON.stringify(listParamter), function(data) {
		dwr.util.removeAllRows('list-table');
		dwr.util.addRows('list-table', data.content, listCell);
		updatePage(data);

		listData = data.content;

		$('tbody tr:even').addClass("alt-row");
		$('a[rel*=modal]').facebox();
	});
};

$(window).load(function() {
	// 标签栏点击事件
	$('.journey-unverified').click(function() {
		window.location.href = 'journey.html#unverified';
	});
	$('.journey-unpassed').click(function() {
		window.location.href = 'journey.html#unpassed';
	});
	$('.journey-passed').click(function() {
		window.location.href = 'journey.html#passed';
	});

	updateList(0, 3);
});
