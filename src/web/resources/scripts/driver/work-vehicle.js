var listParamter = {
	page : 0
};
var listData = null;

var listCell = [ function(data) {
	switch (data.classification) {
	case 0:
		return '小型微型非营运载客车辆';
	case 1:
		return '载货、大型中型非营运载客车辆';
	case 2:
		return '营运载客车辆';
	}
}, function(data) {
	return data.licensePlate;
}, function(data) {
	return data.model;
}, function(data) {
	return dateString(data.registrationDate);
}, function(data) {
	var date = annualCheck.date(data.registrationDate, data.classification);
	var dateText = dateString(date);
	var redSpan = document.createElement('span');
	// 对于一个月内需要年检的加粗加红，并弹出警告
	if (date.getTime() - new Date().getTime() < 2592000000) {
		redSpan.setAttribute('style', 'color: #ff0000; font-weight: bold;');
		showMessage($('h2'), data.licensePlate + ' 需要于 ' + dateText + ' 年检，请及时进行年检。', 'attention');
	}
	redSpan.appendChild(document.createTextNode(dateText));
	return redSpan;
} ];

// 更新列表
var updateList = function(page) {
	if (undefined != page) {
		listParamter.page = page;
	}
	DriverWorkService.getVehicleList(JSON.stringify(listParamter), function(data) {
		dwr.util.removeAllRows('list-table');
		dwr.util.addRows('list-table', data.content, listCell);
		updatePage(data);

		listData = data.content;

		$('tbody tr:even').addClass("alt-row");
		$('a[rel*=modal]').facebox();
	});
};

// 计算车辆年检时间
var annualCheck = {
	date : function(registrationDate, classification) {
		if (null == registrationDate) {
			return '未知';
		}

		var halfYear = 15768000000;

		var date = new Date();
		var currentYear = date.getFullYear();
		var regYear = registrationDate.getFullYear();

		var distance = date.getTime() - registrationDate.getTime();
		var yearDistance = currentYear - regYear;

		if (1 == classification) {
			// 载货汽车和大型、中型非营运载客汽车10年以内每年检验1次；超过10年的，每6个月检验1次
			if (distance <= 20 * halfYear) {
				distance = this.yearDistanceFix(distance, yearDistance);
			} else {
				distance = this.halfYearDistanceFix(distance, yearDistance);
			}
		} else if (2 == classification) {
			// 营运载客汽车5年以内每年检验1次；超过5年的，每6个月检验1次
			if (distance <= 10 * halfYear) {
				distance = this.yearDistanceFix(distance, yearDistance);
			} else {
				distance = this.halfYearDistanceFix(distance, yearDistance);
			}
		} else {
			// 小型、微型非营运载客汽车6年以内每2年检验1次；超过6年的，每年检验1次；超过15年的，每6个月检验1次
			if (distance <= 12 * halfYear) {
				if ((yearDistance) % 2 == 0) {
					distance = this.twoYearDistanceFix(distance, yearDistance);
				} else {
					distance = this.yearDistanceFix(distance, yearDistance);
				}
			} else if (distance <= 30 * halfYear) {
				distance = this.yearDistanceFix(distance, yearDistance);
			} else {
				distance = this.halfYearDistanceFix(distance, yearDistance);
			}
		}
		date.setTime(registrationDate.getTime() + distance);
		return date;
	},
	halfYear : 15768000000,
	twoYearDistanceFix : function(distance, yearDistance) {
		return distance > yearDistance * 2 * this.halfYear ? (yearDistance + 2) * 2 * this.halfYear
				: yearDistance * 2 * this.halfYear;
	},
	yearDistanceFix : function(distance, yearDistance) {
		return distance > yearDistance * 2 * this.halfYear ? (yearDistance + 1) * 2 * this.halfYear
				: yearDistance * 2 * this.halfYear;
	},
	halfYearDistanceFix : function(distance, yearDistance) {
		return distance > yearDistance * 2 * this.halfYear ? (yearDistance * 2 + 1) * this.halfYear
				: yearDistance * 2 * this.halfYear;
	}
};

$(window).load(function() {
	// 标签栏点击事件
	$('.work-journey').click(function() {
		window.location.href = 'driver-work-journey.html';
	});

	updateList(0);
});
