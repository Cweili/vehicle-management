var listParamter = {
	page : 0
};
var listData = null;

var listCell = [ function(data) {
	return data.journeyTime;
}, function(data) {
	return data.route;
}, function(data) {
	if (data.vehicle) {
		return data.vehicle.licensePlate;
	} else {
		return '未定';
	}
}, function(data) {
	return data.remark;
} ];

// 更新列表
var updateList = function(page) {
	if (undefined != page) {
		listParamter.page = page;
	}
	DriverWorkService.getJourneyList(JSON.stringify(listParamter), function(data) {
		dwr.util.removeAllRows('list-table');
		dwr.util.addRows('list-table', data.content, listCell);
		updatePage(data);

		listData = data.content;

		$('tbody tr:even').addClass("alt-row");
		$('a[rel*=modal]').facebox();
	});
};

$(window).load(function() {
	// 标签栏点击事件
	$('.work-vehicle').click(function() {
		window.location.href = 'driver-work-vehicle.html';
	});

	updateList(0);
});
