$(function() {

	// When a menu item with no sub menu is clicked...
	// $("#main-nav li a.no-submenu").click(function() {
	// // Just open the link instead of a sub menu
	// window.location.href = (this.href);
	// return false;
	// });

	// Sidebar Accordion Menu Hover Effect:

	$("#main-nav li .nav-top-item").hover(function() {
		$(this).stop().animate({
			paddingRight : "25px"
		}, 200);
	}, function() {
		$(this).stop().animate({
			paddingRight : "15px"
		});
	});

	// 最小化 Content Box
	// 隐藏 .close-box 的 .content-box-content
	$(".closed-box .content-box-content").hide();
	// 隐藏 .closed-box 的 .content-box-tabs
	$(".closed-box .content-box-tabs").hide();
	// 点击标题时事件
	$(".content-box-header h3").click(function() {
		// 切换隐藏状态
		$(this).parent().next().toggle();
		$(this).parent().parent().toggleClass("closed-box");
		$(this).parent().find(".content-box-tabs").toggle();
	});

	// Content box 标签
	// 隐藏所有标签
	$('.content-box .content-box-content div.tab-content').hide();
	// 添加 .current 到默认标签（.default-tab）
	$('ul.content-box-tabs li a.default-tab').addClass('current');
	// 显示默认标签（.default-tab）
	$('.content-box-content div.default-tab').show();
	// 标签点击事件
	$('.content-box ul.content-box-tabs li a').click(function() {
		// 移除所有 .current
		$(this).parent().siblings().find("a").removeClass('current');
		// 添加 .current 到点击的标签
		$(this).addClass('current');
		// 显示 点击的标签 href 属性指定的 div
		var currentTab = $(this).attr('href');
		$(currentTab).siblings().hide();
		$(currentTab).show();
		return false;
	});

	// 关闭按钮
	// 含 .close 的链接关闭父节点
	$(".close").click(function() {
		$(this).parent().fadeTo(400, 0, function() {
			$(this).slideUp(400);
		});
		return false;
	});

	// checkbox 全选
	$('.check-all').click(
			function() {
				$(this).parent().parent().parent().parent().find(':checkbox').prop('checked',
						$(this).prop('checked'));
			});

	// 含 rel="modal" 的链接点击显示窗口
	$('a[rel*=modal]').facebox();

	// Initialise jQuery UI DatePicker:

	// $.datepicker.setDefaults({
	// showWeek: true,
	// firstDay: 1,
	// numberOfMonths: 1,
	// changeMonth: true,
	// changeYear: true
	// });
	//		
	// $('.datepicker').livequery('focus', function () {
	// $(this).datepicker();
	// });

});

$(window).load(function() {

	sidebarNavigation.update();

	dwr.util.useLoadingMessage('载入中，请稍候...');

	// 点击事件
	$('.button-logout').click(function() {
		CommonService.logout(function(data) {
			if (data) {
				window.location.href = 'login.html';
			}
		});
	});
	$('.button-editor').click(function() {
		clearEditor();
	});

	$('.button-del').click(function() {
		del();
	});
});

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [ o[this.name] ];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

/** Chinese initialisation for the jQuery UI date picker plugin. */
/** Written by Cloudream (cloudream@gmail.com). */
// $(function($) {
// $.datepicker.regional['zh-CN'] = {
// closeText : '关闭',
// prevText : '&#x3c;上月',
// nextText : '下月&#x3e;',
// currentText : '今天',
// monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月',
// '十一月', '十二月' ],
// monthNamesShort : [ '一', '二', '三', '四', '五', '六', '七', '八', '九', '十', '十一',
// '十二' ],
// dayNames : [ '星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六' ],
// dayNamesShort : [ '周日', '周一', '周二', '周三', '周四', '周五', '周六' ],
// dayNamesMin : [ '日', '一', '二', '三', '四', '五', '六' ],
// weekHeader : '周',
// dateFormat : 'yy-mm-dd',
// firstDay : 1,
// isRTL : false,
// showMonthAfterYear : true,
// yearSuffix : ''
// };
// $.datepicker.setDefaults($.datepicker.regional['zh-CN']);
// });
var sidebarNavigation = {
	update : function() {
		var locationURL = window.location.pathname + window.location.search + window.location.hash;
		// 对 href 属性与当前 url 相同的列表项添加 .current
		$("#main-nav li ul li a").each(function(i) {
			var $it = $(this);
			$it.click(function() {
				sidebarNavigation.addCurrent($it);
			});
			if (locationURL.indexOf($it.attr("href")) > -1) {
				sidebarNavigation.addCurrent($it);
				return;
			}
		});

		// 侧边栏导航
		// 全部隐藏
		$("#main-nav li ul").hide();
		// 显示当前子菜单
		$("#main-nav li a.current").parent().find("ul").slideToggle("slow");
		// 顶级菜单点击事件
		$("#main-nav li a.nav-top-item").click(function() {
			// 上滑其他菜单
			$(this).parent().siblings().find("ul").slideUp("normal");
			// 下滑子菜单
			$(this).next().slideToggle("normal");
			return false;
		});
	},
	addCurrent : function(nav) {
		nav.parent().siblings().find('a').removeClass('current');
		nav.addClass('current');
		nav.parent().parent().parent().children(':eq(0)').addClass('current');
	}
};

// 清除编辑框内容
var clearEditor = function() {
	$('#editor').find('input[type="hidden"]').val('');
	$('#editor').find('input[type="text"]').val('');
	$('#editor').find('textarea').text('');
	$('#editor').find('input[type="radio"]').attr('checked', false);
	$('#editor').find('input[value="0"]').attr('checked', 'checked');
};

// 生成操作按钮
var oprIcons = function(id, editIcon, delIcon) {
	var iconDiv = document.createElement('div');
	if (editIcon) {
		var iconA = document.createElement('a');
		iconA.setAttribute('href', '#editor');
		iconA.setAttribute('onclick', 'edit(' + id + ');');
		iconA.setAttribute('title', '编辑');
		iconA.setAttribute('rel', 'modal');
		var iconImg = document.createElement('img');
		iconImg.setAttribute('src', 'resources/images/icons/pencil.png');
		iconImg.setAttribute('alt', '编辑');
		iconA.appendChild(iconImg);
		iconDiv.appendChild(iconA);
	}
	if (delIcon) {
		var iconA = document.createElement('a');
		iconA.setAttribute('onclick', 'del(' + id + ');');
		iconA.setAttribute('title', '删除');
		var iconImg = document.createElement('img');
		iconImg.setAttribute('src', 'resources/images/icons/cross.png');
		iconImg.setAttribute('alt', '删除');
		iconA.appendChild(iconImg);
		iconDiv.appendChild(iconA);
	}
	return iconDiv;
};

// 更新分页
var updatePage = function(page) {

	var current = page.number + 1;
	var fix = page.totalPages - current > 4 ? 4 : 9 - page.totalPages + current;
	var begin = current - fix < 1 ? 1 : current - fix;
	var end = begin + 9 > page.totalPages ? page.totalPages : begin + 9;

	$('.pagination').html('');
	if (page.firstPage) {
		$('.pagination').append(
				'<a class="number">&laquo; 首页</a>' + '<a class="number">&lt; 上页</a>');
	} else {
		$('.pagination').append(
				'<a class="number active" onclick="updateList(0);">&laquo; 首页</a>'
						+ '<a class="number active" onclick="updateList(' + (page.number - 1)
						+ ');">&lt; 上页</a>');
	}

	for ( var i = begin; i <= end; ++i) {
		var pageA = '<a class="number active';
		if (i == current) {
			pageA += ' current';
		} else {
			pageA += '" onclick="updateList(' + (i - 1) + ");";
		}
		pageA += '">' + i + '</a>';
		$('.pagination').append(pageA);
	}

	if (page.lastPage) {
		$('.pagination').append(
				'<a class="number">下页 &gt;</a>' + '<a class="number">末页 &raquo;</a>');
	} else {
		$('.pagination').append(
				'<a class="number active" onclick="updateList(' + current + ');">下页 &gt;</a>'
						+ '<a class="number active" onclick="updateList(' + (page.totalPages - 1)
						+ ');">末页 &raquo;</a>');
	}
};

// 生成日期字符串
var dateString = function(date) {
	return date.getFullYear() + ' 年 ' + (date.getMonth() + 1) + ' 月 ' + date.getDate() + ' 日 ';
};

// 删除
var del = function(id) {
	var ids = [];
	if (id) {
		ids.push(id);
	} else {
		$('input[name="idCheckbox"]').each(function() {
			if ($(this).is(':checked')) {
				ids.push($(this).val());
			}
		});
	}
	delCustom(ids);
};

// 显示提示
var showMessage = function(elem, msg, type) {
	if (!msg) {
		msg = '';
	}
	if (!type) {
		type = 'information';
	}
	var msgDiv = '<div class="notification png_bg ' + type + '"><a href="#" class="close">'
			+ '<img src="resources/images/icons/cross_grey_small.png" title="关闭" alt="关闭" />'
			+ '</a><div>' + msg + '</div></div>';
	elem.append(msgDiv);
	setTimeout(function() {
		$('.notification').fadeOut(1000);
	}, 3000);
};
