<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"
	trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:html>
	<t:head pagetitle="运营中的班次 - 车辆班次">
		<t:script src="service/interface/JourneyService.js" />
		<t:script src="resources/scripts/normal/journey-available.js" />
	</t:head>
	<t:body>
		<t:h2>
			车辆班次：运营中的班次
			<a class="button" href="journey.html#add">申请班次</a>
		</t:h2>
		
		<div class="content-box"><!-- Start Content Box -->
			<div class="content-box-header">
				<h3>运营中的班次列表</h3>
				<ul class="content-box-tabs">
					<li><a class="journey-available default-tab" href="#available">运营中</a></li>
					<li><a class="journey-unverified" href="#unverified">未审核</a></li>
					<li><a class="journey-unpassed" href="#unpassed">审核未通过</a></li>
					<li><a class="journey-passed" href="#passed">审核已通过</a></li>
				</ul>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->
			<div class="content-box-content">
				<div class="tab-content default-tab">
					<table>
						<thead>
							<tr>
								<th>时间</th>
								<th>路线</th>
								<th>司机</th>
								<th>车辆</th>
								<th>备注</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="5">
									<div class="bulk-actions align-left">
										<a class="button" href="journey.html#add">
											申请班次
										</a>
									</div>
									<div class="pagination"></div>
									<div class="clear"></div>
								</td>
							</tr>
						</tfoot>
						<tbody id="list-table"></tbody>
					</table>
				</div> <!-- End .default-tab -->
			</div> <!-- End .content-box-content -->
		</div> <!-- End .content-box -->
	</t:body>
</t:html>
