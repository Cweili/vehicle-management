<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"
	trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:html>
	<t:head pagetitle="车辆管理">
		<t:script src="service/interface/AdminVehicleService.js" />
		<t:script src="resources/scripts/admin/vehicle.js" />
	</t:head>
	<t:body>
		<t:h2 button="添加车辆">车辆管理</t:h2>
		
		<div class="content-box"><!-- Start Content Box -->
			<div class="content-box-header">
				<h3>车辆列表</h3>
				<ul class="content-box-tabs">
					<li><a class="vehicle-small default-tab" href="#small">小型微型非营运载客车辆</a></li>
					<li><a class="vehicle-large" href="#large">载货、大型中型非营运载客车辆</a></li>
					<li><a class="vehicle-commerical" href="#commerical">营运载客车辆</a></li>
				</ul>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->
			<div class="content-box-content">
				<div class="tab-content default-tab">
					<table>
						<thead>
							<tr>
								<th><input class="check-all" type="checkbox" /></th>
								<th>车牌号</th>
								<th>车辆品牌型号</th>
								<th>注册登记日期</th>
								<th>购入日期</th>
								<th>价格</th>
								<th>操作</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="7">
									<div class="bulk-actions align-left">
										<a class="button button-del">
											删除所选项目
										</a>
										<a class="button button-editor" href="#editor" rel="modal">
											添加车辆
										</a>
									</div>
									<div class="pagination"></div>
									<div class="clear"></div>
								</td>
							</tr>
						</tfoot>
						<tbody id="list-table"></tbody>
					</table>
				</div> <!-- End .default-tab -->
			</div> <!-- End .content-box-content -->
		</div> <!-- End .content-box -->
		<t:editor>
			<p>
				<label><em></em>类型</label>
				<input type="radio" name="classification" value="0" checked="checked" /> 小型微型非营运载客车辆
				<input type="radio" name="classification" value="1" /> 载货、大型中型非营运载客车辆
				<input type="radio" name="classification" value="2" /> 营运载客车辆
			</p>
			<t:text name="licensePlate" label="车牌号" required="true" />
			<t:text name="model" label="车辆品牌型号" />
			<p>
				<label>注册登记日期</label>
				<input class="text-input small-input" type="text" name="registrationDateYear" /> 年
				<input class="text-input small-input" type="text" name="registrationDateMonth" /> 月
				<input class="text-input small-input" type="text" name="registrationDateDate" /> 日
			</p>
			<p>
				<label>购入日期</label>
				<input class="text-input small-input" type="text" name="buyDateYear" /> 年
				<input class="text-input small-input" type="text" name="buyDateMonth" /> 年
				<input class="text-input small-input" type="text" name="buyDateDate" /> 日
			</p>
			<t:text style="medium" name="price" label="价格">元</t:text>
			<t:text name="frameNumber" label="车架编号" />
			<t:text name="engineNumber" label="引擎编号" />
		</t:editor>
	</t:body>
</t:html>
