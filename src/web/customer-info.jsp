<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"
	trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:html>
	<t:head pagetitle="个人资料">
		<t:script src="service/interface/CustomerService.js" />
		<t:script>
			$(window).load(function() {
				
				$('a.button').click(function() {
					CustomerService.save(JSON.stringify($('form').serializeObject()), function(data) {
						if (data) {
							showMessage($('form'), '保存成功', 'success');
						} else {
							showMessage($('form'), '保存失败', 'error');
						}
					});
				});
				
				CustomerService.getCustomer(function(data) {
					dwr.util.setValues(data);
					dwr.util.setValues({
						'joinDate' : dateString(data.joinDate),
						'birthday' : dateString(data.birthday),
					});
				});
			});
		</t:script>
	</t:head>
	<t:body>
		<div class="content-box"><!-- Start Content Box -->
			<div class="content-box-header">
				<h3>个人资料</h3>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->
			<div class="content-box-content">
				<div class="tab-content default-tab">
					<form>
						<fieldset>
							<t:text style="medium" name="passwordNew" label="密码">
								<small>留空则不修改</small>
							</t:text>
							<t:text style="medium" name="phone" label="电话" />
							<t:text style="medium" name="number" label="工号" disabled="true">
								<small>请联系管理员修改</small>
							</t:text>
							<p>
								<label>类型</label>
								<input type="radio" name="classification" value="0" checked="checked" disabled="disabled" /> 普通用户
								<input type="radio" name="classification" value="1" disabled="disabled" /> 司机用户
								<input type="radio" name="classification" value="9" disabled="disabled" /> 管理员
							</p>
							<t:text style="medium" name="name" label="姓名" disabled="true">
								<small>请联系管理员修改</small>
							</t:text>
							<p>
								<label>性别</label>
								<input type="radio" name="sex" value="0" checked="checked" disabled="disabled" /> 男
								<input type="radio" name="sex" value="1" disabled="disabled" /> 女
							</p>
							<t:text style="medium" name="joinDate" label="入职日期" disabled="true">
								<small>请联系管理员修改</small>
							</t:text>
							<t:text style="medium" name="birthday" label="出生日期" disabled="true">
								<small>请联系管理员修改</small>
							</t:text>
							<t:text style="medium" name="idCard" label="有效证件" disabled="true">
								<small>请联系管理员修改</small>
							</t:text>
							<p class="align-center">
								<a class="button">保 &nbsp; 存</a>
							</p>
						</fieldset>
						<div class="clear"></div>
					</form>
				</div> <!-- End .default-tab -->
			</div> <!-- End .content-box-content -->
		</div> <!-- End .content-box -->
	</t:body>
</t:html>
