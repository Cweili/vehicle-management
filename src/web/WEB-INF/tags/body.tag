<%@ tag language="java" pageEncoding="UTF-8" isELIgnored="false" trimDirectiveWhitespaces="true" %>
<%@ tag body-content="scriptless" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<body>
	<div id="body-wrapper">
		<t:sidebar />
		<div id="main-content">
			<jsp:doBody />	
			<div id="footer">
				<small>
					&copy; 2013 Bunny | <a href="#">Top</a>
				</small>
			</div><!-- End #footer -->
		</div> <!-- End #main-content -->
	</div>
</body>