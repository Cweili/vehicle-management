<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ tag body-content="empty" %>
<%@ attribute name="url" required="true" %>
<script type="text/javascript">
	setTimeout(function() {
		window.location='${url}';
	}, 3000);
</script>