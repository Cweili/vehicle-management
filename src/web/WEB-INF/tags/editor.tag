<%@ tag language="java" pageEncoding="UTF-8" isELIgnored="false" trimDirectiveWhitespaces="true" %>
<%@ tag body-content="scriptless" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ attribute name="idneedless" required="false" %>
<div id="editor" style="display: none">
	<form id="editor-form">
		<t:if test="${null == idneedless}">
			<input type="hidden" name="id" />
		</t:if>
		<jsp:doBody />
		<p class="align-center">
			<a class="button" onclick="save();">保 &nbsp; 存</a>
		</p>
	</form>
</div> <!-- End #editor -->