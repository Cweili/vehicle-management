<%@ tag language="java" pageEncoding="UTF-8" isELIgnored="false" trimDirectiveWhitespaces="true" %>
<%@ tag body-content="scriptless" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<div id="sidebar">
	<div id="sidebar-wrapper">
		<h1 id="sidebar-title"><a href="#">车辆管理系统</a></h1>
		<a href="#"><img id="logo" src="resources/images/logo.png" /></a>
		<div id="profile-links">
			欢迎您, <a href="customer-info.html" title="修改个人资料">${customer.name}</a>
			<br />
			<br />
			<a class="button-logout">退出登录</a>
		</div>        
		<ul id="main-nav">
			<%--
			<li>
				<a href="index.html" class="nav-top-item no-submenu">车辆管理系统首页</a>       
			</li>
			--%>
			<li>
				<a class="nav-top-item">个人中心</a>
				<ul>
					<li><a href="customer-info.html">个人资料</a></li>
					<t:if test="${customer.classification == 1}">
						<li><a href="driver-work-journey.html">班次安排</a></li>
						<li><a href="driver-work-vehicle.html">车辆安排</a></li>
					</t:if>
				</ul>
			</li>
			<t:if test="${customer.classification < 9}">
				<li>
					<a class="nav-top-item">车辆班次</a>
					<ul>
						<li><a class="normal-journey-available" href="journey-available.html">运营中班次</a></li>
						<li><a class="normal-journey-add" href="journey.html#add">申请班次</a></li>
						<li><a class="normal-journey-unverified" href="journey.html#unverified">未审核班次</a></li>
						<li><a class="normal-journey-unpassed" href="journey.html#unpassed">审核未通过班次</a></li>
						<li><a class="normal-journey-passed" href="journey.html#passed">审核已通过班次</a></li>
					</ul>
				</li>
			</t:if>
			<t:if test="${customer.classification == 1}">
				<li>
					<a class="nav-top-item">我提交的费用</a>
					<ul>
						<li><a class="driver-expense-add" href="driver-expense.html#add">添加一笔费用</a></li>
						<li><a class="driver-expense-unverified" href="driver-expense.html#unverified">未审核费用</a></li>
						<li><a class="driver-expense-unpassed" href="driver-expense.html#unpassed">审核未通过费用</a></li>
						<li><a class="driver-expense-passed" href="driver-expense.html#passed">审核已通过费用</a></li>
					</ul>
				</li>
			</t:if>
			<t:if test="${customer.classification == 9}">
				<li> 
					<a class="nav-top-item">用户管理</a>
					<ul>
						<li><a class="admin-customer-add" href="admin-customer.html#add">添加新用户</a></li>
						<li><a class="admin-customer-normal" href="admin-customer.html#normal">普通用户管理</a></li>
						<li><a class="admin-customer-driver" href="admin-customer.html#driver">司机用户管理</a></li>
						<li><a class="admin-customer-admin" href="admin-customer.html#admin">管理员用户管理</a></li>
					</ul>
				</li>
				<li>
					<a class="nav-top-item">班次管理</a>
					<ul>
						<li><a class="admin-journey-add" href="admin-journey.html#add">添加新班次</a></li>
						<li><a class="admin-journey-available" href="admin-journey.html#available">运营中</a></li>
						<li><a class="admin-journey-unverified" href="admin-journey.html#unverified">未审核</a></li>
						<li><a class="admin-journey-unpassed" href="admin-journey.html#unpassed">审核未通过</a></li>
						<li><a class="admin-journey-unavailable" href="admin-journey.html#unavailable">已不可用</a></li>
					</ul>
				</li>
				<li>
					<a class="nav-top-item">车辆管理</a>
					<ul>
						<li><a class="admin-vehicle-add" href="admin-vehicle.html#add">添加车辆</a></li>
						<li><a class="admin-vehicle-small" href="admin-vehicle.html#small">小型微型非营运载客车辆</a></li>
						<li><a class="admin-vehicle-large" href="admin-vehicle.html#large">载货、大型中型非营运载客</a></li>
						<li><a class="admin-vehicle-commerical" href="admin-vehicle.html#commerical">营运载客车辆</a></li>
					</ul>
				</li>
				<li>
					<a class="nav-top-item">费用管理</a>
					<ul>
						<li><a class="admin-expense-unverified" href="admin-expense.html#unverified">未审核费用</a></li>
						<li><a class="admin-expense-unpassed" href="admin-expense.html#unpassed">审核未通过费用</a></li>
						<li><a class="admin-expense-passed" href="admin-expense.html#passed">审核已通过费用</a></li>
					</ul>
				</li>
			</t:if>
		</ul>
	</div>
</div>