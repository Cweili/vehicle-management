<%@ tag language="java" pageEncoding="UTF-8" isELIgnored="false" trimDirectiveWhitespaces="true" %>
<%@ tag body-content="scriptless" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ attribute name="label" required="true" %>
<%@ attribute name="name" required="true" %>
<%@ attribute name="required" required="false" %>
<%@ attribute name="style" required="false" %>
<%@ attribute name="disabled" required="false" %>
<p>
	<label>
		<t:if test="${null != required}"><em></em></t:if>
		${label}
	</label>
	<input class="text-input <t:if test="${null == style}" otherwise="${style}">large</t:if>-input"
		type="text" name="${name}"
		<t:if test="${null != disabled}">
			disabled="disabled"
		</t:if>
	/>
	<jsp:doBody />
</p>