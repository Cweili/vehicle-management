<%@ tag language="java" pageEncoding="UTF-8" isELIgnored="false" trimDirectiveWhitespaces="true" %>
<%@ tag body-content="scriptless" %>
<%@ attribute name="test" required="true" type="java.lang.Boolean" %>
<%@ attribute name="otherwise" required="false" %>
<% if (null != test && test) { %>
	<jsp:doBody />
<% } else if (null != otherwise) { %>
	${otherwise}
<% } %>