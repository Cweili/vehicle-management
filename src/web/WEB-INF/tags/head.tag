<%@ tag language="java" pageEncoding="UTF-8" isELIgnored="false" trimDirectiveWhitespaces="true" %>
<%@ tag body-content="scriptless" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ attribute name="pagetitle" required="true" %>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>${pagetitle} - 车辆管理系统</title>
	<link rel="stylesheet" href="resources/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resources/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resources/css/invalid.css" type="text/css" media="screen" />
	<t:script src="resources/scripts/jquery-1.9.1.min.js" />
	<t:script src="resources/scripts/facebox.js" />
	<t:script src="resources/scripts/common.js" />
	<t:script src="service/engine.js" />
	<t:script src="service/util.js" />
	<t:script src="service/interface/CommonService.js" />
	<jsp:doBody />
</head>