<%@ tag language="java" pageEncoding="UTF-8" isELIgnored="false" trimDirectiveWhitespaces="true" %>
<%@ tag body-content="scriptless" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ attribute name="button" required="false" %>
<h2>
	<jsp:doBody />
	<t:if test="${null != button}">
		<a class="button button-editor" href="#editor" rel="modal">${button}</a>
	</t:if>
</h2>