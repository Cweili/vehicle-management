<%@ tag language="java" pageEncoding="UTF-8" isELIgnored="false" trimDirectiveWhitespaces="true" %>
<%@ tag body-content="scriptless" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ attribute name="src" required="false" %>
<script type="text/javascript<t:if test="${null != src}">" src="${src}</t:if>"><jsp:doBody /></script>