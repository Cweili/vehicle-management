<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"
	trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:html>
	<t:head pagetitle="用户管理">
		<t:script src="service/interface/AdminCustomerService.js" />
		<t:script src="resources/scripts/admin/customer.js" />
	</t:head>
	<t:body>
		<t:h2 button="添加新用户">用户管理</t:h2>
		
		<div class="content-box"><!-- Start Content Box -->
			<div class="content-box-header">
				<h3>用户列表</h3>
				<ul class="content-box-tabs">
					<li><a class="customer-normal default-tab" href="#normal">普通用户</a></li>
					<li><a class="customer-driver" href="#driver">司机</a></li>
					<li><a class="customer-admin" href="#admin">管理员</a></li>
				</ul>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->
			<div class="content-box-content">
				<div class="tab-content default-tab">
					<table>
						<thead>
							<tr>
								<th><input class="check-all" type="checkbox" /></th>
								<th>姓名</th>
								<th>工号</th>
								<th>性别</th>
								<th>入职日期</th>
								<th>有效证件</th>
								<th>电话</th>
								<th>操作</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="8">
									<div class="bulk-actions align-left">
										<a class="button button-del">
											删除所选项目
										</a>
										<a class="button button-editor" href="#editor" rel="modal">
											添加新用户
										</a>
									</div>
									<div class="pagination"></div>
									<div class="clear"></div>
								</td>
							</tr>
						</tfoot>
						<tbody id="list-table"></tbody>
					</table>
				</div> <!-- End .default-tab -->
			</div> <!-- End .content-box-content -->
		</div> <!-- End .content-box -->
		<t:editor>
			<t:text name="number" label="工号" required="true" />
			<t:text style="medium" name="passwordNew" label="密码">
				<small>留空则不修改</small>
			</t:text>
			<p>
				<label><em></em>类型</label>
				<input type="radio" name="classification" value="0" checked="checked" /> 普通用户
				<input type="radio" name="classification" value="1" /> 司机用户
				<input type="radio" name="classification" value="9" /> 管理员
			</p>
			<t:text name="name" label="姓名" required="true" />
			<p>
				<label><em></em>性别</label>
				<input type="radio" name="sex" value="0" checked="checked" /> 男
				<input type="radio" name="sex" value="1" /> 女
			</p>
			<p>
				<label>入职日期</label>
				<input class="text-input small-input" type="text" name="joinDateYear" /> 年
				<input class="text-input small-input" type="text" name="joinDateMonth" /> 月
				<input class="text-input small-input" type="text" name="joinDateDate" /> 日
			</p>
			<p>
				<label>出生日期</label>
				<input class="text-input small-input" type="text" name="birthdayYear" /> 年
				<input class="text-input small-input" type="text" name="birthdayMonth" /> 年
				<input class="text-input small-input" type="text" name="birthdayDate" /> 日
			</p>
			<t:text name="idCard" label="有效证件" />
			<t:text name="phone" label="电话" />
		</t:editor>
	</t:body>
</t:html>
