<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8" isELIgnored="false"
		 trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:html>
	<t:head pagetitle="班次管理">
		<t:script src="service/interface/AdminJourneyService.js" />
		<t:script src="resources/scripts/admin/journey.js" />
	</t:head>
	<t:body>
		<t:h2 button="添加新班次">班次管理</t:h2>

			<div class="content-box"><!-- Start Content Box -->
				<div class="content-box-header">
					<h3>班次列表</h3>
					<ul class="content-box-tabs">
						<li><a class="journey-available default-tab" href="#available">运营中</a></li>
						<li><a class="journey-unverified" href="#unverified">未审核</a></li>
						<li><a class="journey-unpassed" href="#unpassed">审核未通过</a></li>
						<li><a class="journey-unavailable" href="#unavailable">已不可用</a></li>
					</ul>
					<div class="clear"></div>
				</div> <!-- End .content-box-header -->
				<div class="content-box-content">
					<div class="tab-content default-tab">
						<table>
							<thead>
								<tr>
									<th><input class="check-all" type="checkbox" /></th>
									<th>时间</th>
									<th>路线</th>
									<th>司机</th>
									<th>车辆</th>
									<th>备注</th>
									<th>申请者</th>
									<th>操作</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<td colspan="8">
										<div class="bulk-actions align-left">
											<a class="button button-del">
												删除所选项目
											</a>
											<a class="button button-editor" href="#editor" rel="modal">
												添加新班次
											</a>
										</div>
										<div class="pagination"></div>
										<div class="clear"></div>
									</td>
								</tr>
							</tfoot>
							<tbody id="list-table"></tbody>
						</table>
					</div> <!-- End .default-tab -->
				</div> <!-- End .content-box-content -->
			</div> <!-- End .content-box -->
		<t:editor>
			<t:text name="journeyTime" label="班次时间" required="true" />
			<p>
				<label><em></em>班次路线</label>
			</p>
			<p>
				<textarea class="text-input textarea" name="route" rows="2"></textarea>
			</p>
			<t:text style="medium" name="vehicleLicensePlate" label="车牌号">
				<a class="button" href="admin-vehicle.html#small" target="_blank">车牌号</a>
			</t:text>
			<t:text style="medium" name="driverNumber" label="司机工号">
				<a class="button" href="admin-customer.html#driver" target="_blank">工 &nbsp; 号</a>
			</t:text>
			<p>
				<label>备注说明</label>
			</p>
			<p>
				<textarea class="text-input textarea" name="remark" rows="2"></textarea>
			</p>
			<p>
				<label><em></em>审核状态</label>
				<input type="radio" name="verifyStatus" value="2" /> 已通过审核(运营中)
				<input type="radio" name="verifyStatus" value="1" /> 未通过审核
				<input type="radio" name="verifyStatus" value="3" /> 已不可用
			</p>
		</t:editor>
	</t:body>
</t:html>
