<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"
	trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:html>
	<t:head pagetitle="车辆安排 - 工作安排">
		<t:script src="service/interface/DriverWorkService.js" />
		<t:script src="resources/scripts/driver/work-vehicle.js" />
	</t:head>
	<t:body>
		<t:h2>工作安排：车辆安排</t:h2>
		
		<div class="content-box"><!-- Start Content Box -->
			<div class="content-box-header">
				<h3>车辆安排列表</h3>
				<ul class="content-box-tabs">
					<li><a class="work-journey">班次安排</a></li>
					<li><a class="work-vehicle default-tab">车辆安排</a></li>
				</ul>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->
			<div class="content-box-content">
				<div class="tab-content default-tab">
					<table>
						<thead>
							<tr>
								<th>车辆类型</th>
								<th>车牌号</th>
								<th>车辆品牌型号</th>
								<th>注册登记日期</th>
								<th>下次年检日期</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="5">
									<div class="pagination"></div>
									<div class="clear"></div>
								</td>
							</tr>
						</tfoot>
						<tbody id="list-table"></tbody>
					</table>
				</div> <!-- End .default-tab -->
			</div> <!-- End .content-box-content -->
		</div> <!-- End .content-box -->
	</t:body>
</t:html>
