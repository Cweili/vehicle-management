<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"
	trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:html>
	<t:head pagetitle="班次安排 - 工作安排">
		<t:script src="service/interface/DriverWorkService.js" />
		<t:script src="resources/scripts/driver/work-journey.js" />
	</t:head>
	<t:body>
		<t:h2>工作安排：班次安排</t:h2>
		
		<div class="content-box"><!-- Start Content Box -->
			<div class="content-box-header">
				<h3>班次安排列表</h3>
				<ul class="content-box-tabs">
					<li><a class="work-journey default-tab">班次安排</a></li>
					<li><a class="work-vehicle">车辆安排</a></li>
				</ul>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->
			<div class="content-box-content">
				<div class="tab-content default-tab">
					<table>
						<thead>
							<tr>
								<th>时间</th>
								<th>路线</th>
								<th>车辆</th>
								<th>备注</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="4">
									<div class="pagination"></div>
									<div class="clear"></div>
								</td>
							</tr>
						</tfoot>
						<tbody id="list-table"></tbody>
					</table>
				</div> <!-- End .default-tab -->
			</div> <!-- End .content-box-content -->
		</div> <!-- End .content-box -->
	</t:body>
</t:html>
