<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"
	trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:html>
	<t:head pagetitle="费用管理">
		<t:script src="service/interface/AdminExpenseService.js" />
		<t:script src="resources/scripts/admin/expense.js" />
	</t:head>
	<t:body>
		<t:h2>费用管理</t:h2>
		
		<div class="content-box"><!-- Start Content Box -->
			<div class="content-box-header">
				<h3>费用列表</h3>
				<ul class="content-box-tabs">
					<li><a class="expense-unverified default-tab" href="#unverified">未审核</a></li>
					<li><a class="expense-unpassed" href="#unpassed">审核未通过</a></li>
					<li><a class="expense-passed" href="#passed">审核已通过</a></li>
				</ul>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->
			<div class="content-box-content">
				<div class="tab-content default-tab">
					<table>
						<thead>
							<tr>
								<th><input class="check-all" type="checkbox" /></th>
								<th>种类</th>
								<th>提交时间</th>
								<th>金额</th>
								<th>相关车辆</th>
								<th>提交者</th>
								<th>提交时间</th>
								<th>操作</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="8">
									<div class="bulk-actions align-left">
										<a class="button button-del">
											删除所选项目
										</a>
									</div>
									<div class="pagination"></div>
									<div class="clear"></div>
								</td>
							</tr>
						</tfoot>
						<tbody id="list-table"></tbody>
					</table>
				</div> <!-- End .default-tab -->
			</div> <!-- End .content-box-content -->
		</div> <!-- End .content-box -->
		<t:editor>
			<p>
				<label><em></em>种类</label>
				<input type="radio" name="classification" value="0" checked="checked" /> 燃油费
				<input type="radio" name="classification" value="1" /> 维护保养
				<input type="radio" name="classification" value="2" /> 故障维修
				<input type="radio" name="classification" value="3" /> 年检费
				<input type="radio" name="classification" value="4" /> 保险费
			</p>
			<t:text style="medium" name="price" label="金额" required="true">元</t:text>
			<t:text style="medium" name="vehicleLicensePlate" label="车牌号" required="true" />
			<p>
				<label>备注</label>
			</p>
			<p>
				<textarea class="text-input textarea" name="remark" rows="5"></textarea>
			</p>
			<p>
				<label><em></em>审核状态</label>
				<input type="radio" name="verifyStatus" value="2" /> 已通过审核
				<input type="radio" name="verifyStatus" value="1" /> 未通过审核
			</p>
		</t:editor>
	</t:body>
</t:html>
