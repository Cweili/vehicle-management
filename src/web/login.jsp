<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"
	trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:html>
	<t:head pagetitle="登录">
		<t:script>
			$(window).load(function() {
				dwr.util.useLoadingMessage('登录中，请稍候...');
				$('#login-form').submit(function() {
					return false;
				});
				$('#login-form p input.button').click(function() {
					login();
				});
				$("body").bind('keyup',function(event) {  
					if(event.keyCode==13){
						login();
					}   
				});
			});
			
			var login = function() {
				var loginParamter = dwr.util.getValues('login-form');
				CommonService.login(JSON.stringify(loginParamter), function(data) {
					if (data) {
						$('.notification div').html('<span style="color:#a4d04a">恭喜登录成功，正在进入系统...</span>');
						setTimeout(function() {
							window.location.href = 'customer-info.html';
						}, 2000);
					} else {
						$('.notification div').html('<span style="color:#fe7874">工号或密码错误，请重新登录。</span>');
						$(':password').val('');
						$(':password').focus();
					}
				});
			};
		</t:script>
	</t:head>
	<body id="login">
		<div id="login-wrapper" class="png_bg">
			<div id="login-top">
				<h1>车辆管理系统</h1>
				<!-- Logo (221px width) -->
				<img id="logo" src="resources/images/logo.png" />
			</div>
			<!-- End #logn-top -->
			<div id="login-content">
				<form id="login-form">
					<div class="notification information png_bg">
						<div>欢迎使用车辆管理系统，请登录您的账户。</div>
					</div>
					<p>
						<label>工号</label>
						<input class="text-input" type="text" name="number" tabindex="1" />
					</p>
					<div class="clear"></div>
					<p>
						<label>密码</label>
						<input class="text-input" type="password" name="password" tabindex="2" />
					</p>
					<div class="clear"></div>
					<p id="remember-password">
						<input type="checkbox" tabindex="3" name="remember" value="1" checked="checked" />
						一个月内记住登录状态
					</p>
					<div class="clear"></div>
					<p>
						<input class="button" type="button" value="登 &nbsp; 录" tabindex="4" />
					</p>
				</form>
			</div>
			<!-- End #login-content -->
		</div>
		<!-- End #login-wrapper -->
	</body>
</t:html>