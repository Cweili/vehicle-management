<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"
	trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:html>
	<t:head pagetitle="我申请的班次 - 车辆班次">
		<t:script src="service/interface/JourneyService.js" />
		<t:script src="resources/scripts/normal/journey.js" />
	</t:head>
	<t:body>
		<t:h2 button="申请班次">车辆班次：我申请的班次</t:h2>
		
		<div class="content-box"><!-- Start Content Box -->
			<div class="content-box-header">
				<h3>我申请的班次列表</h3>
				<ul class="content-box-tabs">
					<li><a class="journey-available">运营中</a></li>
					<li><a class="journey-unverified default-tab" href="#unverified">未审核</a></li>
					<li><a class="journey-unpassed" href="#unpassed">审核未通过</a></li>
					<li><a class="journey-passed" href="#passed">审核已通过</a></li>
				</ul>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->
			<div class="content-box-content">
				<div class="tab-content default-tab">
					<table>
						<thead>
							<tr>
								<th><input class="check-all" type="checkbox" /></th>
								<th>时间</th>
								<th>路线</th>
								<th>备注</th>
								<th>审核者</th>
								<th>审核时间</th>
								<th>操作</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="7">
									<div class="bulk-actions align-left">
										<a class="button button-del">
											删除所选项目
										</a>
										<a class="button button-editor" href="#editor" rel="modal">
											申请班次
										</a>
									</div>
									<div class="pagination"></div>
									<div class="clear"></div>
								</td>
							</tr>
						</tfoot>
						<tbody id="list-table"></tbody>
					</table>
				</div> <!-- End .default-tab -->
			</div> <!-- End .content-box-content -->
		</div> <!-- End .content-box -->
		<t:editor>
			<t:text name="journeyTime" label="班次时间" required="true" />
			<p>
				<label><em></em>班次路线</label>
			</p>
			<p>
				<textarea class="text-input textarea" name="route" rows="5"></textarea>
			</p>
			<p>
				<label>备注说明</label>
			</p>
			<p>
				<textarea class="text-input textarea" name="remark" rows="5"></textarea>
			</p>
		</t:editor>
	</t:body>
</t:html>
