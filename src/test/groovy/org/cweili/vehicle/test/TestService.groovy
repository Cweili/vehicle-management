package org.cweili.vehicle.test

import org.apache.commons.lang.StringUtils
import org.cweili.vehicle.entity.Customer
import org.cweili.vehicle.entity.Expense
import org.cweili.vehicle.entity.Journey
import org.cweili.vehicle.entity.Vehicle
import org.cweili.vehicle.service.BaseService
import org.cweili.vehicle.util.Const
import org.directwebremoting.annotations.RemoteProxy
import org.directwebremoting.spring.SpringCreator
import org.springframework.stereotype.Service

import com.alibaba.fastjson.util.Base64

/**
 * 测试服务
 *
 * @author Cweili
 * @version 2013年7月26日 上午12:48:29
 *
 */
@RemoteProxy(creator = SpringCreator.class)
@Service
class TestService extends BaseService {

	List<Customer> importCS092() {

		def line = null
		try {
			new File(this.class.getResource('/cs092.db').path).eachLine {
				line = new String(Base64.decodeFast(it), "UTF-8")
			}
		} catch (Exception e) {
			log.error(e, e)
			return null
		}

		def records = StringUtils.split(line, '\t')
		def params
		def customer
		Calendar calendar = new GregorianCalendar()
		calendar.set(Calendar.MONTH, 0)
		calendar.set(Calendar.DATE, 1)
		for (record in records) {
			params = StringUtils.split(record, ',')

			customer = customerRepository.findByNumber(params[0])
			if (!customer) {
				customer = new Customer()
				customer.joinDate = new Date()
				customer.password = Const.ADMIN_PASSWORD
			}

			customer.number = params[0]
			customer.name = params[1]
			customer.sex = Byte.parseByte(params[2])
			calendar.set(Calendar.YEAR, Integer.parseInt(params[3]))
			customer.birthday = new Date(calendar.getTimeInMillis())
			customer.phone = params[4]
			def dormitory = StringUtils.split(params[5], '#')[1]
			customer.idCard = params[7] + params[3] + "0101" + dormitory + params[2]

			customerRepository.save(customer)
		}

		(List<Customer>) customerRepository.findAll()
	}

	List<Customer> testSaveManyData() {

		def time = System.currentTimeMillis()
		def date = new Date()

		for (int i = 0; i < 1000; i++) {
			def u1 = new Customer()
			u1.joinDate = date
			u1.birthday = date
			u1.idCard = "35010219900303243" + i
			u1.name = "Bunny" + i
			u1.number = "B" + (++time)
			u1.phone = "B" + (++time)

			def u2 = new Customer()
			u2.birthday = date
			u2.joinDate = date
			u2.idCard = "35010219900303243" + i
			u2.name = "Cweili" + i
			u2.number = "C" + (++time)
			u2.phone = "C" + (++time)

			def v = new Vehicle()
			v.buyDate = date
			v.engineNumber = "engine" + time
			v.frameNumber = "frame" + time
			v.licensePlate = "皖B" + ++time
			v.model = "model" + time
			v.price = 0 - (int) ++time
			v.registrationDate = date

			def j = new Journey()
			j.vehicle = v
			j.driver = u1
			j.commitCustomer = u2
			journeyRepository.save(j)

			def e = new Expense()
			e.commitCustomer = u1
			e.vehicle = v
			// expenseRepository.save(e);
		}

		(List<Customer>) customerRepository.findAll()
	}
}
