package org.cweili.vehicle.repository

import org.cweili.vehicle.entity.Customer
import org.cweili.vehicle.entity.Expense
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

/**
 * 费用仓库
 *
 * @author Cweili
 * @version 2013年7月28日 下午7:00:49
 *
 */
interface ExpenseRepository extends BaseRepository<Expense> {

	Page<Expense> findByVerifyStatus(byte verifyStatus, Pageable page)

	Page<Expense> findByCommitCustomerAndVerifyStatus(Customer customer, byte verifyStatus,
	Pageable page)
}
