package org.cweili.vehicle.repository

import org.cweili.vehicle.entity.Customer
import org.cweili.vehicle.entity.Journey
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

/**
 * 班次仓库
 *
 * @author Cweili
 * @version 2013年7月28日 下午7:01:16
 *
 */
interface JourneyRepository extends BaseRepository<Journey> {

	Page<Journey> findByVerifyStatus(byte verifyStatus, Pageable page)

	Page<Journey> findByCommitCustomerAndVerifyStatus(Customer customer, byte verifyStatus, Pageable page)

	Page<Journey> findByDriverAndVerifyStatus(Customer driver, byte verifyStatus, Pageable page)
}
