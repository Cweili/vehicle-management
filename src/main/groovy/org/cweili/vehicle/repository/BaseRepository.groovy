package org.cweili.vehicle.repository

import org.springframework.data.repository.NoRepositoryBean
import org.springframework.data.repository.PagingAndSortingRepository

/**
 * 仓库接口
 *
 * @author Cweili
 * @version 2013年7月28日 下午7:36:57
 *
 * @param <T>
 */
@NoRepositoryBean
interface BaseRepository<T> extends PagingAndSortingRepository<T, Integer> {
}
