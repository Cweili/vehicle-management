package org.cweili.vehicle.repository

import org.cweili.vehicle.entity.Vehicle
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

/**
 * 车辆仓库
 *
 * @author Cweili
 * @version 2013年7月28日 下午7:01:40
 *
 */
interface VehicleRepository extends BaseRepository<Vehicle> {

	Page<Vehicle> findByClassification(byte classification, Pageable pageable)

	Vehicle findByLicensePlate(String licensePlate)
}
