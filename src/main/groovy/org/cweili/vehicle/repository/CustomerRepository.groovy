package org.cweili.vehicle.repository

import org.cweili.vehicle.entity.Customer
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

/**
 * 用户仓库
 *
 * @author Cweili
 * @version 2013年7月28日 下午6:54:29
 *
 */
interface CustomerRepository extends BaseRepository<Customer> {

	Customer findByNumber(String number)

	Page<Customer> findByClassification(byte classification, Pageable pageable)
}
