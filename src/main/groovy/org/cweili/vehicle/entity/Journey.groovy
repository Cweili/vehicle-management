package org.cweili.vehicle.entity

import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Temporal
import javax.persistence.TemporalType

/**
 * 班次
 * 
 * @author Cweili
 * @version 2013-5-16 下午4:01:17
 * 
 */
@Entity
class Journey implements Serializable, Cloneable {

	/**
	 * 班次ID
	 */
	@Id
	@GeneratedValue
	int id

	/**
	 * 时间
	 */
	String journeyTime

	/**
	 * 路线
	 */
	String route

	/**
	 * 车辆
	 */
	@ManyToOne(cascade = [ CascadeType.MERGE, CascadeType.REFRESH ])
	@JoinColumn(name = "vehicle", referencedColumnName = "id")
	Vehicle vehicle

	/**
	 * 司机
	 */
	@ManyToOne(cascade = [ CascadeType.MERGE, CascadeType.REFRESH ])
	@JoinColumn(name = "driver", referencedColumnName = "id")
	Customer driver

	/**
	 * 备注说明
	 */
	String remark

	/**
	 * 申请者
	 */
	@ManyToOne(cascade = [ CascadeType.MERGE, CascadeType.REFRESH ])
	@JoinColumn(name = "commitCustomer", referencedColumnName = "id")
	Customer commitCustomer

	/**
	 * 提交时间
	 */
	@Temporal(TemporalType.DATE)
	Date commitDate

	/**
	 * 审核者
	 */
	@ManyToOne(cascade = [ CascadeType.MERGE, CascadeType.REFRESH ])
	@JoinColumn(name = "verifyCustomer", referencedColumnName = "id")
	Customer verifyCustomer

	/**
	 * 审核时间
	 */
	@Temporal(TemporalType.DATE)
	Date verifyDate

	/**
	 * 审核状态
	 */
	byte verifyStatus

	/**
	 * 未审核
	 */
	public static final byte VERIFY_UNVERIFYED = 0

	/**
	 * 未通过审核
	 */
	public static final byte VERIFY_UNPASSED = 1

	/**
	 * 已通过审核
	 */
	public static final byte VERIFY_PASSED = 2

	/**
	 * 已不可用
	 */
	public static final byte VERIFY_UNAVAILABLE = 3

	@Override
	int hashCode() {
		31 + (int) (id ^ (id >>> 32))
	}

	@Override
	public boolean equals(obj) {
		if (this == obj)
			return true
		if (!obj || !(obj instanceof Journey)) {
			return false
		}
		def other = (Journey) obj
		if (id != other.id)
			return false
		true
	}
}
