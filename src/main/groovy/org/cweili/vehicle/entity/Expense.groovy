package org.cweili.vehicle.entity

import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Temporal
import javax.persistence.TemporalType

/**
 * 费用
 * 
 * @author Cweili
 * @version 2013-5-16 下午4:00:56
 * 
 */
@Entity
class Expense implements Serializable, Cloneable {

	/**
	 * 费用ID
	 */
	@Id
	@GeneratedValue
	int id

	/**
	 * 种类
	 */
	byte classification

	/**
	 * 金额
	 */
	int price

	/**
	 * 相关车辆
	 */
	@ManyToOne(cascade = [CascadeType.MERGE, CascadeType.REFRESH], optional = false)
	@JoinColumn(name = "vehicle", referencedColumnName = "id")
	Vehicle vehicle

	/**
	 * 备注
	 */
	String remark

	/**
	 * 提交者
	 */
	@ManyToOne(cascade = [CascadeType.MERGE, CascadeType.REFRESH], optional = false)
	@JoinColumn(name = "commitCustomer", referencedColumnName = "id")
	Customer commitCustomer

	/**
	 * 提交时间
	 */
	@Temporal(TemporalType.DATE)
	Date commitDate

	/**
	 * 审核者
	 */
	@ManyToOne(cascade = [CascadeType.MERGE, CascadeType.REFRESH])
	@JoinColumn(name = "verifyCustomer", referencedColumnName = "id")
	Customer verifyCustomer

	/**
	 * 审核时间
	 */
	@Temporal(TemporalType.DATE)
	Date verifyDate

	/**
	 * 审核状态
	 */
	byte verifyStatus

	/**
	 * 燃油费
	 */
	public static final byte CLASS_FUEL = 0

	/**
	 * 维护保养
	 */
	public static final byte CLASS_MAINTAIN = 1

	/**
	 * 故障维修
	 */
	public static final byte CLASS_REPAIR = 2

	/**
	 * 年检费
	 */
	public static final byte CLASS_INSPECTION = 3

	/**
	 * 保险费
	 */
	public static final byte CLASS_INSURANCE = 4

	/**
	 * 未审核
	 */
	public static final byte VERIFY_UNVERIFYED = 0

	/**
	 * 未通过审核
	 */
	public static final byte VERIFY_UNPASSED = 1

	/**
	 * 已通过审核
	 */
	public static final byte VERIFY_PASSED = 2

	@Override
	int hashCode() {
		31 + (int) (id ^ (id >>> 32))
	}

	@Override
	boolean equals(Object obj) {
		if (this == obj)
			return true
		if (!obj || !(obj instanceof Expense)) {
			return false
		}
		def other = (Expense) obj
		if (id != other.id)
			return false
		true
	}
}
