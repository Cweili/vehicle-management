package org.cweili.vehicle.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Temporal
import javax.persistence.TemporalType

/**
 * 用户
 * 
 * @author Cweili
 * @version 2013-5-16 下午4:01:42
 * 
 */
@Entity
class Customer implements Serializable, Cloneable {

	/**
	 * 用户ID
	 */
	@Id
	@GeneratedValue
	int id

	/**
	 * 工号
	 */
	@Column(unique = true)
	String number

	/**
	 * 密码
	 */
	String password

	/**
	 * 类型
	 */
	byte classification

	/**
	 * 姓名
	 */
	String name

	/**
	 * 性别
	 */
	byte sex

	/**
	 * 入职日期
	 */
	@Temporal(TemporalType.DATE)
	Date joinDate

	/**
	 * 出生日期
	 */
	@Temporal(TemporalType.DATE)
	Date birthday

	/**
	 * 有效证件
	 */
	String idCard

	/**
	 * 电话
	 */
	String phone

	/**
	 * 用户类型-普通
	 */
	public static final byte CLASS_NORMAL = 0

	/**
	 * 用户类型-司机
	 */
	public static final byte CLASS_DRIVER = 1

	/**
	 * 用户类型-管理员
	 */
	public static final byte CLASS_ADMIN = 9

	/**
	 * 性别-男
	 */
	public static final byte SEX_MALE = 0

	/**
	 * 性别-女
	 */
	public static final byte SEX_FEMALE = 1

	@Override
	int hashCode() {
		31 + (int) (id ^ (id >>> 32))
	}

	@Override
	boolean equals(obj) {
		if (this == obj)
			return true
		if (!obj || !(obj instanceof Customer))
			return false
		def other = (Customer) obj
		if (id != other.id)
			return false
		true
	}
}
