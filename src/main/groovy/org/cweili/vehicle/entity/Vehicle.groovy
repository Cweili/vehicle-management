package org.cweili.vehicle.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Temporal
import javax.persistence.TemporalType

/**
 * 车辆
 * 
 * @author Cweili
 * @version 2013-5-16 下午4:02:17
 * 
 */
@Entity
class Vehicle implements Serializable, Cloneable {

	/**
	 * 车辆ID
	 */
	@Id
	@GeneratedValue
	int id

	/**
	 * 车辆种类
	 */
	byte classification

	/**
	 * 车牌号
	 */
	@Column(unique = true)
	String licensePlate

	/**
	 * 车辆品牌型号
	 */
	String model

	/**
	 * 注册登记日期
	 */
	@Temporal(TemporalType.DATE)
	Date registrationDate

	/**
	 * 购入日期
	 */
	@Temporal(TemporalType.DATE)
	Date buyDate

	/**
	 * 价格
	 */
	int price

	/**
	 * 车架编号
	 */
	String frameNumber

	/**
	 * 引擎编号
	 */
	String engineNumber

	// 小型、微型非营运载客汽车6年以内每2年检验1次；超过6年的，每年检验1次；超过15年的，每6个月检验1次
	public static final byte CLASS_SMALL = 0
	// 载货汽车和大型、中型非营运载客汽车10年以内每年检验1次；超过10年的，每6个月检验1次
	public static final byte CLASS_LARGE = 1
	// 营运载客汽车5年以内每年检验1次；超过5年的，每6个月检验1次
	public static final byte CLASS_COMMERICAL = 2

	@Override
	int hashCode() {
		31 + (int) (id ^ (id >>> 32))
	}

	@Override
	boolean equals(obj) {
		if (this == obj)
			return true
		if (!obj || !(obj instanceof Vehicle)) {
			return false
		}
		def other = (Vehicle) obj
		if (id != other.id)
			return false
		true
	}

}
