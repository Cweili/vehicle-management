package org.cweili.vehicle.util

import org.directwebremoting.event.ScriptSessionEvent
import org.directwebremoting.event.ScriptSessionListener
import org.directwebremoting.impl.DefaultScriptSessionManager

/**
 *
 * @author Cweili
 * @version 2013年7月25日 下午11:52:10
 *
 */
class ScriptSessionManager extends DefaultScriptSessionManager {

	ScriptSessionManager() {
		super()
		def scriptSessionListener = [
			sessionCreated : { ScriptSessionEvent ev -> void },
			sessionDestroyed : { ScriptSessionEvent ev -> void }
		].asType(ScriptSessionListener)
		addScriptSessionListener(scriptSessionListener)
	}
}
