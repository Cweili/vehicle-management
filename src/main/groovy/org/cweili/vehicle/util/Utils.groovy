package org.cweili.vehicle.util

import org.apache.commons.lang.StringUtils

/**
 * 工具方法
 *
 * @author Cweili
 * @version 2013年7月25日 下午11:55:15
 *
 */
class Utils {

	// Empty checks
	// -----------------------------------------------------------------------
	/**
	 * <p>
	 * Checks if a CharSequence after
	 * {@link org.apache.commons.lang.StringUtils#stripToNull(java.lang.String)}
	 * is empty ("") or null.
	 * </p>
	 *
	 * @param strs
	 *            the Strings to check, may be null
	 * @{@code true} if the String is empty or null
	 */
	static boolean isEmpty(String[] strs) {
		for (str in strs) {
			if (StringUtils.isEmpty(StringUtils.stripToNull(str))) {
				return true
			}
		}
		return false
	}

	/**
	 * <p>Case insensitive check if a CharSequence starts with a specified prefix.</p>
	 *
	 * <p>{@code null}s are handled without exceptions. Two {@code null}
	 * references are considered to be equal. The comparison is case insensitive.</p>
	 *
	 * <pre>
	 * StringUtils.startsWithIgnoreCase(null, null)      = true
	 * StringUtils.startsWithIgnoreCase(null, "abc")     = false
	 * StringUtils.startsWithIgnoreCase("abcdef", null)  = false
	 * StringUtils.startsWithIgnoreCase("abcdef", "abc") = true
	 * StringUtils.startsWithIgnoreCase("ABCDEF", "abc") = true
	 * </pre>
	 *
	 * @see java.lang.String#startsWith(String)
	 * @param string  the CharSequence to check, may be null
	 * @param prefix the prefix to find, may be null
	 * @return {@code true} if the CharSequence starts with the prefix, case insensitive, or
	 *  both {@code null}
	 */
	static boolean startsWithIgnoreCase(string, String[] prefixs) {
		for (str in prefixs) {
			if (StringUtils.startsWithIgnoreCase(string, str)) {
				return true
			}
		}
		return false
	}

	/**
	 * Parses money {@code String} to {@code int}
	 * 
	 * @param moneyString The string
	 * @return integer
	 */
	static int stringToMoney(moneyString) {
		def money = 0
		try {
			if (StringUtils.indexOf(moneyString, '.') > 0) {
				def f = Float.parseFloat(moneyString)
				money = (int) (f * 100)
			} else {
				money = Integer.parseInt(moneyString)
				money *= 100
			}
		} catch (Exception e) {
		}
		return money
	}

	/**
	 * Parses the string argument as a signed decimal integer. The
	 * characters in the string must all be decimal digits, except
	 * that the first character may be an ASCII minus sign {@code '-'}
	 * (<code>'&#92;u002D'</code>) to indicate a negative value or an
	 * ASCII plus sign {@code '+'} (<code>'&#92;u002B'</code>) to
	 * indicate a positive value. The resulting integer value is
	 * returned, exactly as if the argument and the radix 10 were
	 * given as arguments to the {@link #parseInt(java.lang.String,
	 * int)} method.
	 *
	 * @param object    a {@code Object} containing the {@code int}
	 *             representation to be parsed
	 * @return     the integer value represented by the argument in decimal.
	 */
	static int parseInt(object) {
		try {
			return Integer.parseInt(object.toString())
		} catch (Exception e) {
			return 0
		}
	}
}
