package org.cweili.vehicle.util

/**
 * 常量
 *
 * @author Cweili
 * @version 2013年7月25日 下午11:40:54
 *
 */
class Const {

	public static final int PAGE_SIZE = 10

	public static final int MAX_AGE = 2592000

	public static final String ADMIN_NUMBER = "3090701224"

	public static final String ADMIN_PASSWORD = "123"

	public static final String SESSION_CUSTOMER = "customer"

	public static final String COOKIE_CUSTOMER = "cweili_vehicle"

	public static final String CHARSET = "UTF-8"

	public static final String[] ALLOW_NOT_LOGIN = [
		"login.html",
		"resources",
		"service/engine.js",
		"service/util.js",
		"service/interface/CommonService.js",
		"service/call/plaincall/__System.",
		"service/call/plaincall/CommonService.login.dwr"
	]

	public static final String[] ADMIN_PREFIX = [
		"admin-",
		"service/interface/Admin",
		"service/call/plaincall/Admin"
	]

	public static final String[] DRIVER_PREFIX = [
		"driver-",
		"service/interface/Driver",
		"service/call/plaincall/Driver"
	]
}
