package org.cweili.vehicle.servlet

import javax.servlet.ServletException
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.apache.commons.lang.StringUtils
import org.cweili.vehicle.entity.Customer
import org.cweili.vehicle.util.Const

/**
 * 页面处理
 *
 * @author Cweili
 * @version 2013年7月28日 下午6:52:29
 *
 */
class PageProcessingServlet extends HttpServlet {

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		process(request, response)
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		process(request, response)
	}

	private process(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		def requestUri = StringUtils
				.substringBeforeLast(
				StringUtils.substringAfter(request.getRequestURI(),
				"${request.getContextPath()}/"), ".html")
		def session = request.getSession(false)

		if (session?.getAttribute(Const.SESSION_CUSTOMER)) {
			def customer = (Customer) session.getAttribute(Const.SESSION_CUSTOMER)
			request.setAttribute("customer", customer)
		}

		request.getRequestDispatcher("${requestUri}.jsp").forward(request, response)
	}
}
