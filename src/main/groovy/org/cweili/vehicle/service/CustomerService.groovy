package org.cweili.vehicle.service

import org.cweili.vehicle.entity.Customer
import org.cweili.vehicle.util.JSON
import org.cweili.vehicle.util.Utils
import org.directwebremoting.annotations.RemoteProxy
import org.directwebremoting.spring.SpringCreator
import org.springframework.stereotype.Service

/**
 * 用户服务
 *
 * @author Cweili
 * @version 2013年7月27日 下午11:09:53
 *
 */
@RemoteProxy(creator = SpringCreator.class)
@Service
class CustomerService extends BaseService {

	@Override
	Customer getCustomer() {
		return super.getCustomer()
	}

	boolean save(String json) {
		def obj = JSON.parseObject(json)

		def phone = JSON.getString(obj, "phone")
		def password = JSON.getString(obj, "passwordNew")

		def customer = getCustomer()
		if (!customer) {
			return false
		}
		customer.phone = phone
		if (!Utils.isEmpty(password)) {
			customer.password = password
		}
		return customerRepository.save(customer)
	}
}
