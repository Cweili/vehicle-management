package org.cweili.vehicle.service

import org.cweili.vehicle.util.Const
import org.cweili.vehicle.util.JSON
import org.cweili.vehicle.util.Utils
import org.directwebremoting.annotations.RemoteProxy
import org.directwebremoting.spring.SpringCreator
import org.springframework.stereotype.Service

/**
 * 公用服务
 *
 * @author Cweili
 * @version 2013年7月26日 下午1:39:11
 *
 */
@RemoteProxy(creator = SpringCreator.class)
@Service
class CommonService extends BaseService {

	boolean login(String json) {
		def obj = JSON.parseObject(json)

		def number = JSON.getString(obj, "number")
		def password = JSON.getString(obj, "password")

		if (!Utils.isEmpty(number, password)) {
			def customer = customerRepository.findByNumber(number)
			if (customer && password.equals(customer.getPassword())) {
				setSessionAttribute(Const.SESSION_CUSTOMER, customer)
				try {
					def array = obj.getJSONArray("remember")
					if (array.size() > 0) {
						setCookie(Const.COOKIE_CUSTOMER, customer, Const.MAX_AGE)
					}
				} catch (Exception e) {
					log.error(e.toString() + json, e)
				}
				return true
			}
		}
		return false
	}

	boolean logout() {
		setSessionAttribute(Const.SESSION_CUSTOMER, null)
		setCookie(Const.COOKIE_CUSTOMER, null, 0)
		return true
	}
}
