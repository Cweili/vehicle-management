package org.cweili.vehicle.service

import org.aspectj.lang.JoinPoint
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before
import org.aspectj.lang.annotation.Pointcut
import org.cweili.vehicle.entity.Customer
import org.cweili.vehicle.util.Const
import org.springframework.stereotype.Component

/**
 * 服务切面
 *
 * @author Cweili
 * @version 2013年7月28日 下午2:24:32
 *
 */
@Component
@Aspect
class ServiceAspect extends BaseService {

	/**
	 * 初始化状态
	 */
	def initialized = false

	@Pointcut("execution (* org.cweili.vehicle.service.*.*(..))")
	void serviceCall() {
	}

	@Pointcut("execution (* org.cweili.vehicle.service.*.*.*(..))")
	void adminServiceCall() {
	}

	@Before("(serviceCall() || adminServiceCall()) && args(json)")
	void beforeServiceCall(JoinPoint joinPoint, String json) {
		// 服务调用日志
		log.info("${joinPoint} ${json}")

		// 初始化管理员
		if (!this.initialized
		&& !customerRepository.findByNumber(Const.ADMIN_NUMBER)) {
			def date = new Date()
			def admin = new Customer(
					number : Const.ADMIN_NUMBER,
					password : Const.ADMIN_PASSWORD,
					name : Const.ADMIN_NUMBER,
					classification : Customer.CLASS_ADMIN,
					birthday : date,
					joinDate : date
					)
			customerRepository.save(admin)
		}
	}
}
